
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto
|| Fecha: 2022.05.18
|| Proyecto: JUNO
|| Proceso: Actualizar la tabla MAESTRO_CARTERA, con los valores de FI PARTIDAS
||				Solo se actualizaran los campos que dependen de estos importes
*/


--=========================================================================================================================================
--														         UPDATE CIERRE CONTABLE - MAESTRO_CARTERA
--=========================================================================================================================================

/*
|| Modificaciones.
||		- El campo ADJ323 y USR060 se calculan de la misma manera
*/

/*
|| Modificaciones
|| Fecha: 2022.05.06
|| Motivo:
|| 	- Provisonalmente se ha creado variable de fecha de cierre contable. Lidia nos ha pedido que la fecha de cierre contable sea de marzo, y no la de abril.
*/

DECLARE @FECHA_CIERRE_CONTABLE VARCHAR(10);
DECLARE @ANYOMES VARCHAR(6);
DECLARE @PERIODO_ANT_ADJ INT, @PERIODO_ANT_ARR INT;


SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @PERIODO_ANT_ADJ = (SELECT MAX(PERIODO) FROM [JUNO].[INF_ADJUDICADOS] WHERE PERIODO != @ANYOMES);
SET @PERIODO_ANT_ARR = (SELECT MAX(PERIODO) FROM [JUNO].[INF_ARRENDAMIENTO] WHERE PERIODO != @ANYOMES);

--=======================================================================
--							TEMPORALES
--=======================================================================

DROP TABLE IF EXISTS #TMP_SOCIEDADES

SELECT CAMPO, 
		 VALOR AS SOCIEDAD
  INTO #TMP_SOCIEDADES
  FROM [JUNO].[TMP_PARAMETRICA]
WHERE INFORME = 'SOCIEDAD'
  AND CONCEPTO = 'KUTXABANK. PERIMETRO BANCOS'
  AND FLAG = 1

--=========================================================================================================================================
--										Tabla Temporal con los datos del Histórico (Repositorio) Mes Anterior ADJUDICADOS
--=========================================================================================================================================

	DROP TABLE IF EXISTS #TMP_JUNO_INF_ADJUDICADOS_PERIODO_ANT
	SELECT TOP 1 WITH TIES
		  INF_ADJ.PERIODO
		, INF_ADJ.USR003 -- SOCIEDAD
		, INF_ADJ.USR004 -- CODIGO ACTIVO (UNIDAD_REGISTRAL)
		, INF_ADJ.CONS10 -- DOTACION RECUPERACION		
		, ROW_NUMBER() OVER(PARTITION BY INF_ADJ.USR003, INF_ADJ.USR004 ORDER BY INF_ADJ.ORIGEN DESC) RN_ADJUDICADOS
		INTO #TMP_JUNO_INF_ADJUDICADOS_PERIODO_ANT
	FROM [JUNO].[INF_ADJUDICADOS] INF_ADJ	
	WHERE INF_ADJ.IND_MIGRACION = 'X'
	  AND INF_ADJ.PERIODO = @PERIODO_ANT_ADJ  
	ORDER BY RN_ADJUDICADOS


 --=========================================================================================================================================
--										Tabla Temporal con los datos de FI_PARTIDAS
--=========================================================================================================================================

DROP TABLE IF EXISTS #TMP_FI_PARTIDAS

SELECT PERIODO,
       SOCIEDAD, 
	   UNIDAD_REGISTRAL,
	   CONS10_DOTACION_RECUPERACION
INTO #TMP_FI_PARTIDAS
  FROM [JUNO].MAESTRO_FI_PARTIDAS_UR
 WHERE PERIODO = @ANYOMES
 

--==========================================================================================================================================
--										UPDATES TABLA MAESTRO_CARTERA
--==========================================================================================================================================


UPDATE CARTERA
SET
CARTERA.CONS10_DOTACION_RECUPERACION          =         ISNULL(ADJ_ANT.CONS10,0) + ISNULL(FI_PARTIDAS.CONS10_DOTACION_RECUPERACION,0)
FROM [JUNO].[MAESTRO_CARTERA] CARTERA
INNER JOIN [JUNO].[TMP_UR_MAESTRO_HIST] UR ON CARTERA.SOCIEDAD = UR.SOCIEDAD AND CARTERA.UNIDAD_REGISTRAL = UR.UNIDAD_REGISTRAL
INNER JOIN #TMP_SOCIEDADES SOC 
  ON CARTERA.SOCIEDAD = SOC.SOCIEDAD
LEFT JOIN #TMP_JUNO_INF_ADJUDICADOS_PERIODO_ANT ADJ_ANT 
  ON CARTERA.USR003_NUM_SOCIEDAD  = ADJ_ANT.USR003 
 AND CARTERA.USR004_CODIGO_ACTIVO = ADJ_ANT.USR004
LEFT JOIN #TMP_FI_PARTIDAS FI_PARTIDAS
  ON CARTERA.SOCIEDAD         = FI_PARTIDAS.SOCIEDAD
 AND CARTERA.UNIDAD_REGISTRAL = FI_PARTIDAS.UNIDAD_REGISTRAL
WHERE CARTERA.PERIODO = @ANYOMES
AND UR.IND_MIGRACION = 'X'

