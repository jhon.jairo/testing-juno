Estos scripts fueron ejecutados como actualización D+4 para el cierre de Agosto de modo excepcional en aras a corregir el valor CONS10,
la funcionalidad del resto de campos es igual a la del proceso original:

--------------------------------------------------
07TMP. JUNO D4 - UPD. CARTERA FI_PARTIDAS BANCOS
--------------------------------------------------

Script correctivo del CONS10 para los activos de Banca

-------------------------------------------
06TMP. JUNO D4 - UPD. CARTERA FI_PARTIDAS
-------------------------------------------

Proceso de update de los campos provinientes de FI_PARTIDAS pero para el cierre de Agosto los activos de Aportación (Sociedad 280)
se tuvieron que calcular como se indica:

Mes Junio (carga Excel 'Carga Aportación CONS10 JUNIO 2022' previa del valor de Junio)
+
Mes Julio (Repositorio)
+
Mes Agosto (FI_PARTIDAS)