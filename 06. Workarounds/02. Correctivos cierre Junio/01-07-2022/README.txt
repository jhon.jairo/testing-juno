

SCRIPT: "00. JUNO. Nuevas URS Perimetro.sql"

	- Se creó en julio para el cierre de junio. Solo se ha ejecutado una vez, para el cierre de junio
	
	- Detalle tareas realizadas:
		00. ESTRCUTURA TABLA TMP_UR_MAESTRO_HIST
			00.1 Crear estructura con los 2 nuevos campos: IND_MIGRACION, DETALLE_MIGRACION
			00.2 Detalles del fichero
				00.2.1 Script: "01. CREATE TMP_UR_MAESTRO_HIST.sql"
				00.2.2 Ruta: "C:\BaseTIS Work\03. SVH\01. Jhon Reyes\10. Otros Proyectos (Tasks)\11. JUNO\03. Desarrollo\01. Create Tables\02. TMP"
				
		01. MODIFICAR PROCESO TMP_UR_MAESTRO_HIST

		02. PROCESO ADHOC New PERIMETRO
		02.1. Crear proceso del nuevo perímetro (deberían ser unas pocas URs)
		02.2. Se añaden 2 nuevos campos:
			02.2.1. IND_MIGRACION
			02.2.2. DETALLE_MIGRACION
		02.3 Campo IND_MIGRACION se marca con 'X' para todas las URs del nuevo perímetro
		02.4 Campo DETALLE_MIGRACION se debe de poner un detalle del porque de este lanzamiento

		03. MODIFICAR PROCESO MAESTRO CARTERA