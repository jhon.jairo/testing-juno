--ADJ122 ADJ122_TIPO_SUELO Nada a Updatear est� todo informado
-- ADJ122 MAESTRO_CARTERA MAYO OK
-- ADJ122 INF_ADJUDICADOS MAYO OK

DROP TABLE IF EXISTS #TMP_TEST_DUPLICADOS

UPDATE CAR
SET ADJ122_TIPO_SUELO = ISNULL(ABRIL.ADJ122,'0')
  FROM SVH_REPORT_DB.JUNO.MAESTRO_CARTERA CAR
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON CAR.USR004_CODIGO_ACTIVO = MAPEO.USR004 AND CAR.USR003_NUM_SOCIEDAD = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ122 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE CAR.PERIODO = 202205 
 AND ADJ122_TIPO_SUELO = ''

 UPDATE ADJUDICADOS
 SET ADJ122  = ISNULL(ABRIL.ADJ122,'0')
 FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS ADJUDICADOS
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON ADJUDICADOS.USR004 = MAPEO.USR004 AND ADJUDICADOS.USR003 = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ122 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE ADJUDICADOS.PERIODO = 202205 
 AND   ADJUDICADOS.ADJ122 = ''







--ADJ067 ADJ067_SISTEMA_GESTION
-- MAESTRO_CARTERA OK
-- INF_ADJUDICADOS OK



SELECT USR004 FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS WHERE PERIODO = 202205 AND ADJ067 = ''

SELECT ADJ067 FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS WHERE PERIODO = 202204 AND USR004 = '15327'

SELECT  CAR.USR004_CODIGO_ACTIVO, USR003_NUM_SOCIEDAD, ADJ323_CORRECCIONES_VALOR_POR_DETERIORO, ABRIL.ADJ323
  FROM SVH_REPORT_DB.JUNO.MAESTRO_CARTERA CAR
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON CAR.USR004_CODIGO_ACTIVO = MAPEO.USR004 AND CAR.USR003_NUM_SOCIEDAD = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ323 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE CAR.PERIODO = 202205 
 AND ADJ323_CORRECCIONES_VALOR_POR_DETERIORO IS NULL

 SELECT * FROM SVH_REPORT_DB.JUNO.MAESTRO_CARTERA



--ADJ068

SELECT COUNT(*) FROM SVH_REPORT_DB.JUNO.MAESTRO_CARTERA WHERE PERIODO = 202205 AND ADJ068_FASE_GESTION = ''

SELECT USR004  FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS WHERE PERIODO = 202205 AND ADJ068 = ''

--BDE019

SELECT COUNT(*) FROM SVH_REPORT_DB.JUNO.MAESTRO_CARTERA WHERE PERIODO = 202205 AND BDE019_PROXIMIDAD_RESPECTO_NUCLEO_URBANO IS NULL OR BDE019_PROXIMIDAD_RESPECTO_NUCLEO_URBANO IS NULL

SELECT USR004  FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS WHERE PERIODO = 202205 AND BDE019 = '' OR BDE019 IS NULL

--ADJ074


SELECT COUNT(*) FROM SVH_REPORT_DB.JUNO.MAESTRO_CARTERA WHERE PERIODO = 202205 AND ADJ074_FECHA_ESTIMADA_TERMINAR_OBRA IS NULL OR ADJ074_FECHA_ESTIMADA_TERMINAR_OBRA = ''

SELECT CAR.USR003_NUM_SOCIEDAD ,CAR.USR004_CODIGO_ACTIVO, CAR.ADJ074_FECHA_ESTIMADA_TERMINAR_OBRA, ABRIL.ADJ074
  FROM #MAESTRO_CARTERA_TEST CAR
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON CAR.USR004_CODIGO_ACTIVO = MAPEO.USR004 AND CAR.USR003_NUM_SOCIEDAD = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ074 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE CAR.PERIODO = 202205 
 AND ISNULL(CAR.ADJ074_FECHA_ESTIMADA_TERMINAR_OBRA,'') = ''


 
UPDATE CAR
SET ADJ074_FECHA_ESTIMADA_TERMINAR_OBRA = CONVERT(DATE, ABRIL.ADJ074,103) 
  FROM  SVH_REPORT_DB.JUNO.MAESTRO_cARTERA CAR
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON CAR.USR004_CODIGO_ACTIVO = MAPEO.USR004 AND CAR.USR003_NUM_SOCIEDAD = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ074 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE CAR.PERIODO = 202205 
 AND ISNULL(CAR.ADJ074_FECHA_ESTIMADA_TERMINAR_OBRA,'') = ''
 
SELECT ADJUDICADOS.USR004, ADJUDICADOS.USR003, ADJUDICADOS.ADJ074, ABRIL.ADJ074
 FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS ADJUDICADOS
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON ADJUDICADOS.USR004 = MAPEO.USR004 AND ADJUDICADOS.USR003 = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ074 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE ADJUDICADOS.PERIODO = 202205 
 AND   ADJUDICADOS.ADJ074 = ''
 AND ISNULL(ABRIL.ADJ074 , '' ) <> ''

--ADJ077


SELECT USR004_CODIGO_ACTIVO FROM #MAESTRO_CARTERA_TEST WHERE PERIODO = 202205 AND ADJ077_COSTE_ESTIMADO_TERMINAR_OBRA IS NULL 

SELECT USR004  FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS WHERE PERIODO = 202205 AND BDE019 = '' OR BDE019 IS NULL

K2661
K3430
14082
16449
77872
69839
78585
79115


UPDATE CAR
SET ADJ077_COSTE_ESTIMADO_TERMINAR_OBRA = ABRIL.ADJ077
  FROM SVH_REPORT_DB.JUNO.MAESTRO_cARTERA CAR
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON CAR.USR004_CODIGO_ACTIVO = MAPEO.USR004 AND CAR.USR003_NUM_SOCIEDAD = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ077 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE CAR.PERIODO = 202205 
 AND ADJ077_COSTE_ESTIMADO_TERMINAR_OBRA IS NULL
  
  --6585


  
  SELECT ADJUDICADOS.USR004, ADJUDICADOS.ADJ077, ABRIL.ADJ077
 FROM #ADJUDICADOS_TEST ADJUDICADOS
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON ADJUDICADOS.USR004 = MAPEO.USR004 AND ADJUDICADOS.USR003 = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ077 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE ADJUDICADOS.PERIODO = 202205 
 AND   ADJUDICADOS.ADJ077 IS NULL

  UPDATE ADJUDICADOS
  SET ADJ077 = ISNULL(ABRIL.ADJ077,0.0)
 FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS ADJUDICADOS
LEFT JOIN (SELECT * FROM SVH_REPORT_DB.JUNO.TMP_MAPEO_URS  ) MAPEO
ON ADJUDICADOS.USR004 = MAPEO.USR004 AND ADJUDICADOS.USR003 = MAPEO.USR003
LEFT JOIN (SELECT USR003, USR004, ADJ077 
             FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS INF
			WHERE INF.PERIODO = 202204
			  AND CONTROL_CARTERA in ( 'Stock', 'Bajas 2022 (ene - abr)','Ventas 2022 (ene - abr)')) ABRIL
ON MAPEO.USR003 = ABRIL.USR003 AND MAPEO.USR004 = ABRIL.USR004
 WHERE ADJUDICADOS.PERIODO = 202205 
 AND   ADJUDICADOS.ADJ077 IS NULL
 


 SELECT USR004 FROM SVH_REPORT_DB.JUNO.INF_ADJUDICADOS WHERE PERIODO = 202205 AND ADJ077 IS NULL