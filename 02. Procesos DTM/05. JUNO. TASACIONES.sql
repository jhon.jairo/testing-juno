
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto
|| Fecha: 2022.03.10
|| Proyecto: JUNO
|| Proceso: Generaci�n de tabla datamart de Tasaciones de la sociedad Kutxabank
*/


/*
|| MODELO JUNO
|| Granularidad: SOCIEDAD + UR + ID_SOLICITUD_TASACION
||	Perimetro:	  Todas las tasaciones V�lidas
*/

/*
|| Modificaci�n: 2022.06.23
|| Autor: Jhon Reyes
|| Motivo:
||		- Ahora el datamart de tasaciones tendra "todo" el universo de las tasaciones validas, es decir, se excluye el filtro de fecha analizada.
||		- El proceso de cartera, necesita que el datamart de tasaciones tenga "todo" el universo de las tasaciones validas
|| Impacto:
||		- El datamart de tasaciones con este cambio, tendr� muchos mas registros, en el testing realizado pasa de 900 registros a 27 mil registros aprox
||		- Se creara un campo para indicar el perimetro "original" del datamart de tasaciones. IND_PERIMETRO
||			- Para generar el informe de tasaciones se deber� filtrar por este campo. IND_PERIMETRO = 'X'
*/



--=========================================================================================================================================
--														         HISTORICOS. TABLAS TEMPORALES - GRANULARIDAD SOCIEDAD + UR
--=========================================================================================================================================

/*
|| Autor: Dani Bieto
|| Fecha: 2022.03.10
|| Proceso: Tablas temporales que van a contener la data a cierre de mes, que se obtienen de las tablas Historicas
*/

DECLARE @FECHA_CORTE_TASACIONES VARCHAR(10);
DECLARE @PERIODO INT;
DECLARE @ANYOMES VARCHAR(6);
DECLARE @ANYOMES_BORRADO VARCHAR(6);

SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);

SET @ANYOMES_BORRADO = CONVERT(VARCHAR(6),REPLACE(EOMONTH(DATEADD(month, -24, GETDATE())),'-',''));

SET @PERIODO = (SELECT MAX(PERIODO) FROM [JUNO].[INF_TASACIONES] WHERE PERIODO != @ANYOMES);
SET @FECHA_CORTE_TASACIONES = '2022-05-15' --CONVERT(VARCHAR(10),DATEADD(DD,-(DAY(GETDATE())+15),GETDATE()),121) -- Dia 15 del mes anterior


--==========================================================================--
--						Temporal Sociedades									--
--==========================================================================--

DROP TABLE IF EXISTS #TMP_SOCIEDADES

SELECT CAMPO, 
		 VALOR AS SOCIEDAD
  INTO #TMP_SOCIEDADES
  FROM [JUNO].[TMP_PARAMETRICA]
WHERE INFORME = 'SOCIEDAD'
  AND CONCEPTO IN('KUTXABANK. PERIMETRO BANCOS', 'KUTXABANK. PERIMETRO INMOBILIARIA')
  AND FLAG = 1

--==========================================================================--
--						MAESTRO TASACIONES  								--
--						DATOS SISTEMA										--
--==========================================================================--

----=================================================----
---- PROCESO DE BORRADO DEL HISTORICO A 2 A�OS VISTA ----
----=================================================----

	DELETE FROM [JUNO].[MAESTRO_TASACIONES] WHERE PERIODO = @ANYOMES_BORRADO

----========================================================----
---- PROCESO DE BORRADO DEL DATAMART EN CASO DE REEJECUCI�N ----
----========================================================----

	DELETE FROM [JUNO].[MAESTRO_TASACIONES] WHERE PERIODO = @ANYOMES


----=======================----
---- INSERT TABLA DATAMART ----
----=======================----

	INSERT INTO [JUNO].[MAESTRO_TASACIONES]
	SELECT --********************************************** TABLA TASACIONES *****************************************************************--
		   @ANYOMES											AS PERIODO,
		   A.SOCIEDAD										AS SOCIEDAD,
		   CAST(A.ID_SOLICITUD_TASACION AS NUMERIC)			AS ID_SOLICITUD_TASACION,
		   A.FECHA_ANALIZADA,
		   A.UNIDAD_REGISTRAL								AS UNIDAD_REGISTRAL,
		   A.VERSION										AS VERSION,
		   CASE WHEN FUSION.UR_ORIGEN_INI != '' AND FUSION.UR_ORIGEN_INI != '0' THEN FUSION.UR_ORIGEN_INI ELSE CAST(A.UNIDAD_REGISTRAL AS VARCHAR) END	AS USR004_CODIGO_ACTIVO,
		   TAS_VALIDAS.VALOR AS ADJ095_NIF_SOCIEDAD_TASACION,
		   A.REFERENCIA_TASADOR                             AS ADJ096_NUMERO_TASACION,
		   A.FECHA_TASACION						            AS ADJ097_FECHA_TASACION,
		   A.IMPORTE_TASACION								AS ADJ098_IMPORTE_TASACION,
		   A.VAL_H_EDIF_T_DEF								AS ADJ099_VALOR_HIPOTESIS_EDIF_TERMINADO,
		   A.VALOR_SUELO									AS ADJ100_VALOR_TERRENO,
		   CASE WHEN A.DESC_TIPO_VALORACION IN ('DOTACI�N ESTAD�STICA', 'VALOR DE LIQUIDACI�N', 'RICS') then 'N' 
		        ELSE 'S' 
		   END											    AS ADJ101_INDICADOR_ORDEN_ECO,
		   A.METODO_VALORACION								AS ADJ102_METODO_VALORACION,
		   CASE WHEN ISNULL(A.CONDICIONANTES,'') <> '' AND A.CONDICIONANTES = '01' THEN 'S'
		        WHEN ISNULL(A.CONDICIONANTES,'') <> '' AND A.CONDICIONANTES = '02' THEN 'N'
				ELSE '' END AS ADJ103_CONDICIONANTES,
		   CASE WHEN ISNULL(A.ADVERTENCIAS,'') <> '' AND A.ADVERTENCIAS = '01' THEN 'S'
		        WHEN ISNULL(A.ADVERTENCIAS,'') <> '' AND A.ADVERTENCIAS = '02' THEN 'N'
				ELSE '' END AS ADJ104_ADVERTENCIAS,
		   CASE WHEN ISNULL(A.VISITA,'') <> '' AND A.VISITA = '01' THEN 'S'
		        WHEN ISNULL(A.VISITA,'') <> '' AND A.VISITA = '02' THEN 'N'
				ELSE '' END AS ADJ407_INDICADOR_VISITA_AL_INMUEBLE,
		   CASE WHEN ISNULL(A.TIPO_DATOS_INMUEBLES_COMPARABLES,'') <> '' AND A.TIPO_DATOS_INMUEBLES_COMPARABLES = 'Datos de oferta' THEN '2'
		        ELSE 0 END AS ADJ408_TIPO_DATOS_INMUEBLES_COMPARABLES,
		   A.TIPO_ACTUALIZACION								AS ADJ409_TIPO_ACTUALIZACION,
		   A.TASA_ANUAL_MEDIA_VAR_PRECIO					AS ADJ410_TASA_ANUAL_MEDIA_VAR_PRECIO,
		   A.PLAZO_MAX_FIN_CONSTRUCCION						AS ADJ411_PLAZO_MAX_FIN_CONSTRUCCION,
		   A.PLAZO_MAX_FIN_COMERCIALIZACION					AS ADJ412_PLAZO_MAX_FIN_COMERCIALIZACION,
		   A.MARGEN_BENEFICIO_PROMOTOR						AS ADJ413_MARGEN_BENEFICIO_PROMOTOR,
		   A.VALOR_CONSTRUCCION								AS ADJ414_VALOR_EFECTOS_SEGURO,
		   A.EMPRESA_TASADORA								AS CODSOCTAS_EMPRESA_TASADORA,
		   CASE WHEN A.DESC_TIPO_VALORACION in ('DOTACI�N ESTAD�STICA') THEN 'M'
                WHEN ISNULL(A.DESC_TIPO_VALORACION,'') = '' THEN 'Z' 
                ELSE 'S'
		   END												AS INDTASAUT_IND_TASACION_AUTOMATICA,
		   A.VALOR_SUELO									AS VALTASSUE_VALOR_SUELO,
		   A.VALOR_VUELO									AS VALTASVUE_VALOR_VUELO,
			A.VALOR_HIPOTECARIO								AS BDE026_VALOR_HIPOTECARIO,
		   A.TASA_ANUALIZADA_HOMOGENEA						AS BDE042_TASA_ANUALIZADA_HOMOGENEA,
		   A.TIPO_VALORACION								AS TIPO_VALORACION,
		   A.DESC_TIPO_VALORACION							AS FINALIDAD_TASACION,
		   A.DESC_ESTADO									AS DESC_ESTADO,
		   A.TASADOR										AS TASADOR,
		   A.DESC_TASADOR									AS DESC_TASADOR,
		   A.DESC_METODO_VALORACION							AS DESC_METODO_VALORACION,
		   A.DIA_NATURAL									AS DIA_NATURAL,
		   A.VALOR_TERRENO_AJUSTADO                         AS VALOR_TERRENO_AJUSTADO,
		   A.FLAG_VISITA_TASACION							AS FLAG_VISITA_TASACION,
		   A.PORCENTAJE_CONSTRUIDO							AS PORCENTAJE_CONSTRUIDO,
		   A.FECHA_ULTIMO_GRADO_AVANCE_ESTIMADO		AS FECHA_ULTIMO_GRADO_AVANCE_ESTIMADO,
		   A.FLAG_FIJAPRECIO								AS FLAG_FIJAPRECIO,
		   A.MOTIVO_REVISION								AS MOTIVO_REVISION,
		   A.SUPERFICIE_REGISTRAL_CONSTRUIDA		AS SUPERFICIE_REGISTRAL_CONSTRUIDA,
		   A.FECHA_TASACION								AS FECHA_TASACION,
		   --********************************************** TABLA TASACIONES_DATOS_ADICIONALES **********************************************--
		   B.ID_SOLICITUD_TASACION							AS ID_SOLICITUD_TASACION_TASDA,
		   B.FECHA_ESTIMADA_TERMINAR_OBRA					AS FECHA_ESTIMADA_TERMINAR_OBRA,
		   B.COSTE_ESTIMADO_TERMINAR_OBRA					AS COSTE_ESTIMADO_TERMINAR_OBRA,
		   B.SISTEMA_GESTION								AS SISTEMA_GESTION,
		   B.FASE_GESTION									AS FASE_GESTION,
		   B.PARALIZACION_URBANIZACION						AS PARALIZACION_URBANIZACION,
		   B.PORCENTAJE_URBANIZACION_EJECUTADO				AS PORCENTAJE_URBANIZACION_EJECUTADO,
		   B.PORCENTAJE_AMBITO_VALORADO						AS PORCENTAJE_AMBITO_VALORADO,
		   B.PROXIMIDAD_NUCLEO_URBANO						AS PROXIMIDAD_NUCLEO_URBANO,
		   B.PROYECTO_OBRA									AS PROYECTO_OBRA,
			B.DESARROLLO_PLANEAMIENTO,
			B.APROVECHAMIENTO,
         B.TIPO_SUELO,
         B.FINCA_RUSTICA_EXPECT_URBANISTICAS,
		   --********************************************** TABLA TASACIONES_METODOS ********************************************************--
		   C.SUPERFICIE_BRUTA_ALQUILADA						AS SUPERFICIE_BRUTA_ALQUILADA,
		   --********************************************** DATOS AUXILIARES ****************************************************************--
			
			--************************************** INDICADOR DEL PERIMETRO DE TASACIONES ***************************************************--
			CASE WHEN A.FECHA_ANALIZADA >= @FECHA_CORTE_TASACIONES THEN 'X' ELSE '' END IND_PERIMETRO
			
	FROM SVH_CORP_DB.DBO.TASACIONES A
	INNER JOIN #TMP_SOCIEDADES SOCIEDADES
	 ON A.SOCIEDAD = SOCIEDADES.SOCIEDAD
	INNER JOIN (SELECT CAMPO, VALOR FROM SVH_REPORT_DB.JUNO.TMP_PARAMETRICA WHERE INFORME = 'TASADORAS_VALIDAS') TAS_VALIDAS
	 ON CAST(A.TASADOR AS INT) = TAS_VALIDAS.CAMPO
    INNER JOIN (SELECT VALOR AS DESC_TIPO_VALORACION FROM SVH_REPORT_DB.JUNO.TMP_PARAMETRICA WHERE INFORME = 'Tasaciones' AND CAMPO = 'DESC_TIPO_VALORACION' AND FLAG = 1) TIPO_VAL 
	 ON A.DESC_TIPO_VALORACION = TIPO_VAL.DESC_TIPO_VALORACION
    LEFT JOIN SVH_CORP_DB.DBO.DATOS_FUSION FUSION 
	 ON A.SOCIEDAD          = FUSION.SOCIEDAD 
	AND A.UNIDAD_REGISTRAL = FUSION.UNIDAD_REGISTRAL	  
	LEFT JOIN SVH_CORP_DB.DBO.TASACIONES_DATOS_ADICIONALES B
	 ON A.SOCIEDAD               = B.SOCIEDAD
	AND A.ID_SOLICITUD_TASACION = B.ID_SOLICITUD_TASACION
	AND A.UNIDAD_REGISTRAL      = B.UNIDAD_REGISTRAL
	LEFT JOIN SVH_CORP_DB.dbo.TASACIONES_METODOS C
  	 ON A.ID_SOLICITUD_TASACION = C.ID_SOLICITUD
	AND A.UNIDAD_REGISTRAL     = C.UNIDAD_REGISTRAL
	AND A.VERSION              = C.VERSION
	WHERE A.DESC_ESTADO      = 'Analizada' 	
	  --AND A.FECHA_ANALIZADA >=  @FECHA_CORTE_TASACIONES


