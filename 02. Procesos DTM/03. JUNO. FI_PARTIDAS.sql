
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto 
|| Fecha: 2022.04.07
|| Proyecto: JUNO
|| Proceso: Generaci�n de tabla datamart FI_PARTIDAS
*/


/*
|| MODELO JUNO
|| Granularidad: PERIODO + SOCIEDAD + UNIDAD_REGISTRAL 
||	Perimetro:	 Todos los clientes de Kutxa
*/
DECLARE @FECHA_FIN_DATOS VARCHAR(10); 
DECLARE @ANYO INT;
DECLARE @ANYO_BORRADO VARCHAR(6);

SET @ANYO = SUBSTRING(CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112),1,4);
SET @ANYO_BORRADO = SUBSTRING(CONVERT(VARCHAR(6),REPLACE(EOMONTH(DATEADD(month, -24, GETDATE())),'-','')),1,4);
SET @FECHA_FIN_DATOS = CONVERT(VARCHAR(10),DATEADD(DD,-(DAY(GETDATE())),GETDATE()),121);	


--==========================================================================--
--						Temporal Sociedades									--
--==========================================================================--

DROP TABLE IF EXISTS #TMP_SOCIEDADES

SELECT CAMPO, 
		 VALOR AS SOCIEDAD
  INTO #TMP_SOCIEDADES
  FROM [JUNO].[TMP_PARAMETRICA]
WHERE INFORME = 'SOCIEDAD'
  AND CONCEPTO IN('KUTXABANK. PERIMETRO BANCOS', 'KUTXABANK. PERIMETRO INMOBILIARIA')
  AND FLAG = 1

--==========================================================================--
--						TEMPORAL MAESTRO FI_PARTIDAS									--
--==========================================================================--

  
DROP TABLE IF EXISTS #TMP_FI_PARTIDAS  -- SVH_REPORT_DB.JUNO.MAESTRO_FI_PARTIDAS_UR

SELECT	YEAR(FECHA_CONTABILIZACION) AS EJERCICIO,
		RIGHT('00'+ CAST(CAST(MONTH(FECHA_CONTABILIZACION) AS NUMERIC(2)) AS VARCHAR(2)),2) AS MES, 
		CAST(CONCAT(YEAR(FECHA_CONTABILIZACION),RIGHT('00'+ CAST(CAST(MONTH(FECHA_CONTABILIZACION) AS NUMERIC(2)) AS VARCHAR(2)),2)) AS INT) AS PERIODO,
		PARTIDAS.SOCIEDAD AS SOCIEDAD, 
		UNIDAD_REGISTRAL,
		UNIDAD_REGISTRAL AS ID_UR_ACTUAL_DIARIA,
		SUM(CASE WHEN PARAM.CONCEPTO = 'Mantenimiento_Seguro'    THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS ADJ149_COSTES_MANTENIMIENTO_SEGURO,
		SUM(CASE WHEN PARAM.CONCEPTO = 'Mantenimiento_Comunidad' THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS ADJ150_COSTES_MANTENIMIENTO_COMUNIDAD,
		SUM(CASE WHEN PARAM.CONCEPTO = 'Mantenimiento_Impuestos' THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS ADJ247_COSTES_MANTENIMIENTO_IMPUESTOS,
		SUM(CASE WHEN PARAM.CONCEPTO = 'Mantenimiento_Gestor'    THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS BDE027_COSTES_MANTENIMIENTO_GESTOR,
		SUM(CASE WHEN PARAM.CONCEPTO = 'Matenimiento_Otros'      THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS ADJ327_COSTES_MANTENIMIENTO_OTROS,
		SUM(CASE WHEN PARAM.CONCEPTO = 'Costes_Imputables'       THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS ADJ466_COSTES_IMPUTABLES_INQUILINOS,
		SUM(CASE WHEN PARAM.CONCEPTO = 'Alquileres_Pendientes_Cobro' AND PARTIDAS.CLASE_DOCUMENTO NOT IN ( 'P0','ZA','ZY') AND (ISNULL(PARTIDAS.FECHA_COMPENSACION,'') = '' OR PARTIDAS.FECHA_COMPENSACION > @FECHA_FIN_DATOS) THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS ADJ180_ALQUILERES_VENCIDOS_PENDIENTES,
		SUM(CASE WHEN PARTIDAS.CUENTA_MAYOR LIKE '0000706%'		 THEN CAST(IMPORTE  AS FLOAT) ELSE 0 END) AS USR089_INGRESOS_HASTA_VENTA,
		SUM(CASE WHEN PARTIDAS.CUENTA_MAYOR LIKE '0000180%'	OR PARTIDAS.CUENTA_MAYOR LIKE '0000181%' THEN CAST(IMPORTE  AS FLOAT) ELSE 0 END) AS ADJ178_FIANZA_RECIBIDA,
		0 AS ADJ179_CANTIDADES_RECIBIDAS,
		SUM(CASE WHEN PARTIDAS.SOCIEDAD NOT IN ('200','201') AND (   PARTIDAS.CUENTA_MAYOR LIKE '0000693%' 
                                                                  OR PARTIDAS.CUENTA_MAYOR LIKE '0000793%'
                                                                  OR PARTIDAS.CUENTA_MAYOR LIKE '0000692%' 
                                                                  OR PARTIDAS.CUENTA_MAYOR LIKE '0000792%') THEN CAST(IMPORTE AS FLOAT) 
                 WHEN PARTIDAS.SOCIEDAD IN ('200','201') AND PARTIDAS.CUENTA_MAYOR IN ('0000692051',
																					   '0000692052',
																					   '0000692053',
																					   '0000692055',
																					   '0000693051',
																					   '0000792051',
																					   '0000792052',
																					   '0000792053',
																					   '0000792055',
																					   '0000793051') THEN  CAST(IMPORTE AS FLOAT)
                 ELSE 0 END) AS CONS10_DOTACION_RECUPERACION,
		SUM(CASE WHEN PARTIDAS.CUENTA_MAYOR = '0000631400' THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS USR077_PRE_ITP,
		SUM(CASE WHEN (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 201 AND PARTIDAS.ELEMENTO_PEP IN ('020100-22-02-AJD_ACT', '020100-22-03-AJD_V','020100-22-03-AJD_S')) OR
				      (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 210 AND PARTIDAS.ELEMENTO_PEP IN ('021000-22-02-AJD_ACT', '021000-22-03-AJD_V','021000-22-03-AJD_S')) OR
					  (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 230 AND PARTIDAS.ELEMENTO_PEP IN ('023000-22-02-AJD_ACT', '023000-22-03-AJD_V','023000-22-03-AJD_S')) OR
					  (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 240 AND PARTIDAS.ELEMENTO_PEP IN ('024000-22-02-AJD_ACT', '024000-22-03-AJD_V','024000-22-03-AJD_S')) OR
					  (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 250 AND PARTIDAS.ELEMENTO_PEP IN ('025000-22-02-AJD_ACT', '025000-22-03-AJD_V','025000-22-03-AJD_S')) OR
					  (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 260 AND PARTIDAS.ELEMENTO_PEP IN ('026000-22-02-AJD_ACT', '026000-22-03-AJD_V','026000-22-03-AJD_S')) OR
					  (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 270 AND PARTIDAS.ELEMENTO_PEP IN ('027000-22-02-AJD_ACT', '027000-22-03-AJD_V','027000-22-03-AJD_S')) OR
                      (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 280 AND PARTIDAS.ELEMENTO_PEP IN ('028000-22-02-AJD_ACT', '028000-22-03-AJD_V','028000-22-03-AJD_S')) OR
					  (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 220 AND PARTIDAS.ELEMENTO_PEP IN ('022000-22-02-AJD_ACT', '022000-22-03-AJD_V','022000-22-03-AJD_S')) OR
					  (PARTIDAS.CUENTA_MAYOR = '0000631400' AND PARTIDAS.SOCIEDAD = 200 AND PARTIDAS.ELEMENTO_PEP IN ('020000-22-02-AJD_ACT', '020000-22-03-AJD_V','020000-22-03-AJD_S')) THEN CAST(IMPORTE  AS FLOAT) ELSE 0 END) AS USR078_AJD,
		SUM(CASE WHEN PARTIDAS.CUENTA_MAYOR =  '0000629100' OR PARTIDAS.CUENTA_MAYOR = '0000629188' THEN CAST(IMPORTE  AS FLOAT) ELSE 0 END) AS USR079_SANEAMIENTO_ADJUDICACION_COMUNIDADES,
		SUM(CASE WHEN PARTIDAS.CUENTA_MAYOR =  '0000631200'  THEN CAST(IMPORTE  AS FLOAT) ELSE 0 END) AS USR080_SANEAMIENTO_ADJUDICACION_IBI,
		SUM(CASE WHEN PARTIDAS.CUENTA_MAYOR =  '0000631399'  THEN CAST(IMPORTE  AS FLOAT) ELSE 0 END) AS USR099_PLUSVALIA_POR_ADJUDICACION,
        SUM(CASE WHEN PARTIDAS.CUENTA_MAYOR = '0000430%' THEN CAST(IMPORTE AS FLOAT) ELSE 0 END) AS USR094_IMPORTE_COBRO_APLAZADO
	INTO #TMP_FI_PARTIDAS -- SVH_REPORT_DB.JUNO.MAESTRO_FI_PARTIDAS_UR
	FROM [SVH_CORP_DB].[DBO].[FI_PARTIDAS_UR] PARTIDAS
	INNER JOIN #TMP_SOCIEDADES SOCIEDADES
	  ON PARTIDAS.SOCIEDAD = SOCIEDADES.SOCIEDAD
	LEFT JOIN SVH_REPORT_DB.JUNO.TMP_PARAMETRICA AS PARAM
	  ON PARTIDAS.CUENTA_MAYOR = PARAM.VALOR
	WHERE MONTH(FECHA_CONTABILIZACION) < MONTH(GETDATE()) 
	  AND YEAR(FECHA_CONTABILIZACION) >= YEAR(GETDATE())
	GROUP BY PARTIDAS.SOCIEDAD, UNIDAD_REGISTRAL, YEAR(FECHA_CONTABILIZACION), MONTH(FECHA_CONTABILIZACION)

	

--==========================================================================--
--						 MAESTRO FI_PARTIDAS								--
--==========================================================================--

----=================================================----
---- PROCESO DE BORRADO DEL HISTORICO A 2 A�OS VISTA ----
----=================================================----

	DELETE FROM [JUNO].[MAESTRO_FI_PARTIDAS_UR] WHERE EJERCICIO = @ANYO_BORRADO

----========================================================----
---- PROCESO DE BORRADO DEL DATAMART EN CASO DE REEJECUCI�N ----
----========================================================----

	DELETE FROM [JUNO].[MAESTRO_FI_PARTIDAS_UR] WHERE EJERCICIO = @ANYO


----=======================----
---- INSERT TABLA DATAMART ----
----=======================----

	INSERT INTO [JUNO].[MAESTRO_FI_PARTIDAS_UR]
	SELECT PARTIDAS.EJERCICIO,
		   PARTIDAS.MES,
		   PARTIDAS.PERIODO,
		   PARTIDAS.SOCIEDAD,
		   PARTIDAS.UNIDAD_REGISTRAL,
		   PARTIDAS.ID_UR_ACTUAL_DIARIA,
		   PARTIDAS.ADJ149_COSTES_MANTENIMIENTO_SEGURO,
		   PARTIDAS.ADJ150_COSTES_MANTENIMIENTO_COMUNIDAD,
		   PARTIDAS.ADJ247_COSTES_MANTENIMIENTO_IMPUESTOS,
		   PARTIDAS.BDE027_COSTES_MANTENIMIENTO_GESTOR,
		   PARTIDAS.ADJ327_COSTES_MANTENIMIENTO_OTROS,
		   PARTIDAS.ADJ466_COSTES_IMPUTABLES_INQUILINOS,
		   PARTIDAS.ADJ180_ALQUILERES_VENCIDOS_PENDIENTES,
		   PARTIDAS.USR089_INGRESOS_HASTA_VENTA,
		   PARTIDAS.ADJ178_FIANZA_RECIBIDA,
		   PARTIDAS.ADJ179_CANTIDADES_RECIBIDAS,
		   PARTIDAS.CONS10_DOTACION_RECUPERACION,
	       PARTIDAS.USR077_PRE_ITP - USR078_AJD AS USR077_ITP,
		   PARTIDAS.USR078_AJD,
		   PARTIDAS.USR079_SANEAMIENTO_ADJUDICACION_COMUNIDADES,
		   PARTIDAS.USR080_SANEAMIENTO_ADJUDICACION_IBI,
		   PARTIDAS.USR099_PLUSVALIA_POR_ADJUDICACION,
           PARTIDAS.USR094_IMPORTE_COBRO_APLAZADO
	FROM #TMP_FI_PARTIDAS PARTIDAS

	--9820


	