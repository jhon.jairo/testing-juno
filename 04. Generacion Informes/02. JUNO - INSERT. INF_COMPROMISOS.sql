
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto
|| Fecha: 2022.05.02
|| Proyecto: JUNO
|| Proceso: Insert tabla final Repositorio de Compromisos
*/


/*
|| MODELO JUNO
|| Granularidad: SOCIEDAD + UR
||	Perimetro:	  Todas los Compromisos
*/

DECLARE @ANYOMES VARCHAR(6);
DECLARE @ANYOMES_BORRADO VARCHAR(6);
DECLARE @PERIODO_DATAMART VARCHAR(6);

SET @PERIODO_DATAMART = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES_BORRADO = CONVERT(VARCHAR(6),REPLACE(EOMONTH(DATEADD(month, -24, GETDATE())),'-',''))

--=========================================================================================================================================
--														         HISTORICOS. TABLAS TEMPORALES - GRANULARIDAD SOCIEDAD + UR
--=========================================================================================================================================



--=========================================
-- Borrado Hist�rico (Se mantienen 2 a�os de hist�rico)
--=========================================
DELETE FROM SVH_REPORT_DB.JUNO.INF_COMPROMISOS
WHERE PERIODO = @ANYOMES_BORRADO


/*
|| Si existen registros del mes actual, se borran.
|| Con el objetivo de poder validar desde el d�a D, es que este script se ejecutar� 2 veces.
||		1. El d�a D, se cargar�n los datos de sistema + repositorio. Esto ya se puede validar y detectar cuanto antes posibles errores.
||		2. El d�a D+4, se cargar�n los datos de sistema + repositorio + logistica + update cierre contable
|| Entre la primera y segunda ejecuci�n es que se realizar� el delete
*/

DELETE FROM [JUNO].INF_COMPROMISOS WHERE PERIODO = @PERIODO_DATAMART




INSERT INTO SVH_REPORT_DB.JUNO.INF_COMPROMISOS
 SELECT @ANYOMES AS PERIODO,
		isnull(PROYECTO_IDENTIFICACION_PROYECTO,''),
		isnull(TIPO,''),
		isnull(CLAVE1,''),
		isnull(CLAVE2,''),
		isnull(CLAVE3,''),
		isnull(CLAVE4,''),
		isnull(ADJ317_CODIGO_ACTIVO,''),
		isnull(ADJ437_ID_PERSONA_COMPRADOR,''),
		isnull(ADJ438_NOMBRE_PERSONA,''),
		REPLACE(isnull(ADJ439_FECHA_CONTRATO_PRIVADO,''),'1900-01-01',''),
		isnull(ADJ440_PRECIO_COMPROMETIDO_COMPRAVENTA,0),
		isnull(ADJ441_IMPORTE_ARRAS,0)
FROM SVH_REPORT_DB.JUNO.MAESTRO_COMPROMISOS
WHERE PERIODO = @ANYOMES