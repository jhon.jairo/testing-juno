
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto
|| Fecha: 2022.05.02
|| Proyecto: JUNO
|| Proceso: Insert tabla final Repositorio de Arrendamientos
*/


/*
|| MODELO JUNO
|| Granularidad: SOCIEDAD + UA + CONTRATO
||	Perimetro:	  Todas las Personas
*/

DECLARE @ANYOMES VARCHAR(6);
DECLARE @ANYOMES_BORRADO VARCHAR(6);
DECLARE @PERIODO_DATAMART VARCHAR(6);

SET @PERIODO_DATAMART = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES_BORRADO = CONVERT(VARCHAR(6),REPLACE(EOMONTH(DATEADD(month, -24, GETDATE())),'-',''))


--=========================================================================================================================================
--														         HISTORICOS. TABLAS TEMPORALES - GRANULARIDAD SOCIEDAD + UR
--=========================================================================================================================================



--=========================================
-- Borrado Hist�rico (Se mantienen 2 a�os de hist�rico)
--=========================================

DELETE FROM SVH_REPORT_DB.JUNO.INF_ARRENDAMIENTO
WHERE PERIODO = @ANYOMES_BORRADO

/*
|| Si existen registros del mes actual, se borran.
|| Con el objetivo de poder validar desde el d�a D, es que este script se ejecutar� 2 veces.
||		1. El d�a D, se cargar�n los datos de sistema + repositorio. Esto ya se puede validar y detectar cuanto antes posibles errores.
||		2. El d�a D+4, se cargar�n los datos de sistema + repositorio + logistica + update cierre contable
|| Entre la primera y segunda ejecuci�n es que se realizar� el delete
*/

DELETE FROM [JUNO].[INF_ARRENDAMIENTO] WHERE PERIODO = @PERIODO_DATAMART




INSERT INTO SVH_REPORT_DB.JUNO.INF_ARRENDAMIENTO
 SELECT @ANYOMES AS PERIODO,
		isnull(PROYECTO_IDENTIFICACION_PROYECTO,''),
		isnull(TIPO,''),
		isnull(CLAVE1,''),
		isnull(CLAVE2,''),
		isnull(CLAVE3,''),
		isnull(CLAVE4,''),
		isnull(ADJ317_CODIGO_ACTIVO,''),
		isnull(ADJ365_ID_ARRENDATARIO,''),
		isnull(ADJ464_NOMBRE_ARRENDATARIO,''),
		isnull(ADJ173_ID_UNIDAD_ALQUILADA,''),
		isnull(ADJ174_FECHA_INICIO_CONTRATO_ALQUILER,''),
		isnull(ADJ175_FECHA_VENCIMIENTO_CONTRATO_ALQUILER,''),
		isnull(ADJ176_OPCION_RENOVACION,''),
		isnull(ADJ177_OPCION_COMPRA,''),
		isnull(ADJ465_IMPORTE_BRUTO_ANUAL,0),
		isnull(ADJ178_FIANZA_RECIBIDA,0),
		isnull(ADJ179_CANTIDADES_RECIBIDAS,0),
		isnull(ADJ466_COSTES_IMPUTABLES_INQUILINOS,0),
		isnull(ADJ180_ALQUILERES_VENCIDOS_PENDIENTES,0)
FROM SVH_REPORT_DB.JUNO.MAESTRO_UA_CONTRATOS
WHERE PERIODO = @ANYOMES