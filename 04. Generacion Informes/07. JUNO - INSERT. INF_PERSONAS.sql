
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto
|| Fecha: 2022.05.02
|| Proyecto: JUNO
|| Proceso: Insert tabla final Repositorio de Personas
*/


/*
|| MODELO JUNO
|| Granularidad: SOCIEDAD + UR + ID_SOLICITUD_TASACION
||	Perimetro:	  Todas las Personas
*/

DECLARE @ANYOMES VARCHAR(6);
DECLARE @PERIODO INT;
DECLARE @ANYOMES_BORRADO VARCHAR(6);
DECLARE @PERIODO_DATAMART VARCHAR(6);

SET @PERIODO_DATAMART = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @PERIODO = (SELECT MAX(PERIODO) FROM [JUNO].[INF_PERSONAS] WHERE PERIODO != @ANYOMES);
SET @ANYOMES_BORRADO = CONVERT(VARCHAR(6),REPLACE(EOMONTH(DATEADD(month, -24, GETDATE())),'-',''))

--=========================================================================================================================================
--														         HISTORICOS. TABLAS TEMPORALES - GRANULARIDAD SOCIEDAD + UR
--=========================================================================================================================================



--=========================================
-- Borrado Hist�rico (Se mantienen 2 a�os de hist�rico)
--=========================================

DELETE FROM SVH_REPORT_DB.JUNO.INF_PERSONAS
WHERE PERIODO = @ANYOMES_BORRADO

/*
|| Si existen registros del mes actual, se borran.
|| Con el objetivo de poder validar desde el d�a D, es que este script se ejecutar� 2 veces.
||		1. El d�a D, se cargar�n los datos de sistema + repositorio. Esto ya se puede validar y detectar cuanto antes posibles errores.
||		2. El d�a D+4, se cargar�n los datos de sistema + repositorio + logistica + update cierre contable
|| Entre la primera y segunda ejecuci�n es que se realizar� el delete
*/

DELETE FROM [JUNO].[INF_PERSONAS] WHERE PERIODO = @PERIODO_DATAMART

INSERT INTO SVH_REPORT_DB.JUNO.INF_PERSONAS
 SELECT 'SISTEMA' AS ORIGEN,
        PERIODO ,
		isnull(ADJ488_ID_PERSONA,''),
		isnull(ADJ489_NOMBRE,''),
		isnull(ADJ490_PAIS,''),
		isnull(ADJ491_PROVINCIA,''),
		isnull(ADJ492_POBLACION,''),
		isnull(ADJ493_TIPO_VIA,''),
		isnull(ADJ494_NOMBRE_VIA,''),
		isnull(ADJ495_NUMERO,''),
		isnull(ADJ496_BLOQUE,''),
		isnull(ADJ497_PLANTA,''),
		isnull(ADJ498_PUERTA,''),
		isnull(ADJ499_CODIGO_POSTAL,''),
		isnull(ADJ500_TELEFONO,'')
FROM SVH_REPORT_DB.JUNO.MAESTRO_PERSONAS
WHERE PERIODO = @ANYOMES
--Pendiente nos confirmen como seleccionar las personas de Venta del repositorio
/*UNION ALL
 SELECT 'REPOSITORIO' AS ORIGEN,
        @ANYOMES AS PERIODO,
		ADJ488,
		ADJ489,
		ADJ490,
		ADJ491,
		ADJ492,
		ADJ493,
		ADJ494,
		ADJ495,
		ADJ496,
		ADJ497,
		ADJ498,
		ADJ499,
		ADJ500
FROM SVH_REPORT_DB.JUNO.INF_PERSONAS
WHERE PERIODO = @PERIODO*/
