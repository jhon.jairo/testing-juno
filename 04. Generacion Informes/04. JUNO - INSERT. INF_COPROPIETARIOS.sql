
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto
|| Fecha: 2022.05.02
|| Proyecto: JUNO
|| Proceso: Insert tabla final Repositorio de Copropietario
*/


/*
|| MODELO JUNO
|| Granularidad: ID_PERSONA_COPROPIETARIA
||	Perimetro:	  Todas los copropietarios
*/

DECLARE @ANYOMES VARCHAR(6);
DECLARE @PERIODO INT;
DECLARE @ANYOMES_BORRADO VARCHAR(6);
DECLARE @PERIODO_DATAMART VARCHAR(6);

SET @PERIODO_DATAMART = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @PERIODO = (SELECT MAX(PERIODO) FROM [JUNO].[INF_COPROPIETARIOS] WHERE PERIODO != @ANYOMES);
SET @ANYOMES_BORRADO = CONVERT(VARCHAR(6),REPLACE(EOMONTH(DATEADD(month, -24, GETDATE())),'-',''))

--=========================================================================================================================================
--														         HISTORICOS. TABLAS TEMPORALES - GRANULARIDAD SOCIEDAD + UR
--=========================================================================================================================================



--=========================================
-- Borrado Hist�rico (Se mantienen 2 a�os de hist�rico)
--=========================================

DELETE FROM SVH_REPORT_DB.JUNO.INF_COPROPIETARIOS
WHERE PERIODO = @ANYOMES_BORRADO

/*
|| Si existen registros del mes actual, se borran.
|| Con el objetivo de poder validar desde el d�a D, es que este script se ejecutar� 2 veces.
||		1. El d�a D, se cargar�n los datos de sistema + repositorio. Esto ya se puede validar y detectar cuanto antes posibles errores.
||		2. El d�a D+4, se cargar�n los datos de sistema + repositorio + logistica + update cierre contable
|| Entre la primera y segunda ejecuci�n es que se realizar� el delete
*/

DELETE FROM [JUNO].[INF_COPROPIETARIOS] WHERE PERIODO = @PERIODO_DATAMART


INSERT INTO SVH_REPORT_DB.JUNO.INF_COPROPIETARIOS
-- DATOS SISTEMA
 SELECT 'SISTEMA' AS ORIGEN,
		@ANYOMES AS PERIODO,
		ISNULL(PROYECTO_IDENTIFICACION_PROYECTO,''),
		ISNULL(TIPO,''),
		ISNULL(CLAVE1,''),
		ISNULL(CLAVE2,''),
		ISNULL(CLAVE3,''),
		ISNULL(CLAVE4,''),
		ISNULL(ADJ317_CODIGO_ACTIVO,''),
		ISNULL(ADJ421_ID_PERSONA_COPROPIETARIA,''),
		ISNULL(ADJ489_NOMBRE_PERSONA,''),
		ISNULL(ADJ423_PORC_INMUEBLE_COPROPIETARIO,0)
FROM SVH_REPORT_DB.JUNO.MAESTRO_COPROPIETARIOS
WHERE PERIODO = @ANYOMES
-- DATOS REPOSITORIO
UNION ALL
 SELECT 'REPOSITORIO' AS ORIGEN,
        @ANYOMES AS PERIODO,
		PROYECTO,
		TIPO,
		CLAVE1,
		CLAVE2,
		CLAVE3,
		CLAVE4,
		ADJ317,
		ADJ421,
		ADJ489,
		ADJ423
FROM SVH_REPORT_DB.JUNO.INF_COPROPIETARIOS
WHERE PERIODO = @PERIODO