
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto
|| Fecha: 2022.05.02
|| Proyecto: JUNO
|| Proceso: Insert tabla final Repositorio de Tasaciones
*/


/*
|| MODELO JUNO
|| Granularidad: SOCIEDAD + UR  + ID SOLICITUD TASACION
||	Perimetro:	  Todas las Personas
*/

DECLARE @ANYOMES VARCHAR(6);
DECLARE @PERIODO INT;
DECLARE @PERIODO_DATAMART INT;
DECLARE @ANYOMES_BORRADO VARCHAR(6);

SET @PERIODO_DATAMART = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @PERIODO = (SELECT MAX(PERIODO) FROM [JUNO].[INF_TASACIONES] WHERE PERIODO != @ANYOMES);
SET @ANYOMES_BORRADO = CONVERT(VARCHAR(6),REPLACE(EOMONTH(DATEADD(month, -24, GETDATE())),'-',''))

--=========================================================================================================================================
--														         HISTORICOS. TABLAS TEMPORALES - GRANULARIDAD SOCIEDAD + UR
--=========================================================================================================================================


/*
|| Si existen registros del mes actual, se borran.
|| Con el objetivo de poder validar desde el d�a D, es que este script se ejecutar� 2 veces.
||		1. El d�a D, se cargar�n los datos de sistema + repositorio. Esto ya se puede validar y detectar cuanto antes posibles errores.
||		2. El d�a D+4, se cargar�n los datos de sistema + repositorio + logistica + update cierre contable
|| Entre la primera y segunda ejecuci�n es que se realizar� el delete
*/

DELETE FROM [JUNO].[INF_TASACIONES] WHERE PERIODO = @PERIODO_DATAMART

--=========================================
-- Borrado Hist�rico (Se mantienen 2 a�os de hist�rico)
--=========================================

DELETE FROM SVH_REPORT_DB.JUNO.INF_TASACIONES
WHERE PERIODO = @ANYOMES_BORRADO

INSERT INTO SVH_REPORT_DB.JUNO.INF_TASACIONES
-- DATOS SISTEMA
 SELECT 'SISTEMA' AS ORIGEN,
		@ANYOMES AS PERIODO,
		isnull(USR004_CODIGO_ACTIVO,''),
		isnull(CAST(ADJ097_FECHA_TASACION AS VARCHAR(10)),'') AS ADJ097_FECHA_TASACION, 
		isnull(ADJ096_NUMERO_TASACION,''),
		isnull(CODSOCTAS_EMPRESA_TASADORA,''),
		isnull(ADJ095_NIF_SOCIEDAD_TASACION,''),
		isnull(ADJ102_METODO_VALORACION,''),
		isnull(ADJ101_INDICADOR_ORDEN_ECO,''),
		isnull(ADJ103_CONDICIONANTES,''),
		isnull(ADJ104_ADVERTENCIAS,''),
		isnull(ADJ407_INDICADOR_VISITA_AL_INMUEBLE,''),
		isnull(INDTASAUT_IND_TASACION_AUTOMATICA,''),
		isnull(ADJ408_TIPO_DATOS_INMUEBLES_COMPARABLES,''),
		isnull(ADJ409_TIPO_ACTUALIZACION,''),
		isnull(ADJ410_TASA_ANUAL_MEDIA_VAR_PRECIO,''),
		isnull(ADJ411_PLAZO_MAX_FIN_CONSTRUCCION,''),
		isnull(ADJ412_PLAZO_MAX_FIN_COMERCIALIZACION,''),
		isnull(ADJ413_MARGEN_BENEFICIO_PROMOTOR,0),
		isnull(ADJ098_IMPORTE_TASACION,0),
		isnull(VALTASVUE_VALOR_VUELO,0),
		isnull(VALTASSUE_VALOR_SUELO,0),
		isnull(BDE026_VALOR_HIPOTECARIO,0),
		isnull(ADJ099_VALOR_HIPOTESIS_EDIF_TERMINADO,0),
		isnull(ADJ100_VALOR_TERRENO,0),
		isnull(ADJ414_VALOR_EFECTOS_SEGURO,0),
		isnull(BDE042_TASA_ANUALIZADA_HOMOGENEA,0),
		isnull(FINALIDAD_TASACION,0)
FROM SVH_REPORT_DB.JUNO.MAESTRO_TASACIONES
WHERE PERIODO = @ANYOMES
AND IND_PERIMETRO = 'X'
-- DATOS REPOSITORIO
UNION ALL
 SELECT 'REPOSITORIO' AS ORIGEN,
        @ANYOMES AS PERIODO,
		USR004,
		ADJ097,
		ADJ096,
		CODSOCTAS,
		ADJ095,
		ADJ102,
		ADJ101,
		ADJ103,
		ADJ104,
		ADJ407,
		INDTASAUT,
		ADJ408,
		ADJ409,
		ADJ410,
		ADJ411,
		ADJ412,
		ADJ413,
		ADJ098,
		VALTASVUE,
		VALTASSUE,
		BDE026,
		ADJ099,
		ADJ100, 
		ADJ414,
		BDE042,
		FINALIDAD
FROM SVH_REPORT_DB.JUNO.INF_TASACIONES
WHERE PERIODO = @PERIODO
AND ORIGEN = 'REPOSITORIO'



