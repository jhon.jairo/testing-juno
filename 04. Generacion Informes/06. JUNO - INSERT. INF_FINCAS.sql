
USE SVH_REPORT_DB
GO

/*
|| Autor: Dani Bieto
|| Fecha: 2022.05.02
|| Proyecto: JUNO
|| Proceso: Insert tabla final Repositorio de Fincas
*/


/*
|| MODELO JUNO
|| Granularidad: SOCIEDAD + UR
||	Perimetro:	  Todas las Fincas
*/

DECLARE @ANYOMES VARCHAR(6);
DECLARE @ANYOMES_BORRADO VARCHAR(6);
DECLARE @PERIODO_DATAMART VARCHAR(6);

SET @PERIODO_DATAMART = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);
SET @ANYOMES_BORRADO = CONVERT(VARCHAR(6),REPLACE(EOMONTH(DATEADD(month, -24, GETDATE())),'-',''))

--=========================================================================================================================================
--														         HISTORICOS. TABLAS TEMPORALES - GRANULARIDAD SOCIEDAD + UR
--=========================================================================================================================================



--=========================================
-- Borrado Hist�rico (Se mantienen 2 a�os de hist�rico)
--=========================================

DELETE FROM SVH_REPORT_DB.JUNO.INF_FINCAS
WHERE PERIODO = @ANYOMES_BORRADO

/*
|| Si existen registros del mes actual, se borran.
|| Con el objetivo de poder validar desde el d�a D, es que este script se ejecutar� 2 veces.
||		1. El d�a D, se cargar�n los datos de sistema + repositorio. Esto ya se puede validar y detectar cuanto antes posibles errores.
||		2. El d�a D+4, se cargar�n los datos de sistema + repositorio + logistica + update cierre contable
|| Entre la primera y segunda ejecuci�n es que se realizar� el delete
*/

DELETE FROM [JUNO].[INF_FINCAS] WHERE PERIODO = @PERIODO_DATAMART

INSERT INTO SVH_REPORT_DB.JUNO.INF_FINCAS
 SELECT @ANYOMES AS PERIODO,
		isnull(PROYECTO_IDENTIFICACION_PROYECTO,''),
		isnull(TIPO,''),
		isnull(CLAVE1,''),
		isnull(CLAVE2,''),
		isnull(CLAVE3,''),
		isnull(CLAVE4,''),
		isnull(ADJ317_CODIGO_ACTIVO,''),
		isnull(ADJ395_IDUFIR,''),		
		CASE WHEN ISNULL(ADJ396_PROVINCIA, '') IN('', '99', 'NR') THEN '0' ELSE ADJ396_PROVINCIA END,		
		isnull(ADJ510_MUNICIPIO,''),
		isnull(ADJ397_NUMERO_REGISTRO,''),
		isnull(ADJ398_TOMO,''),
		isnull(ADJ399_LIBRO,''),
		isnull(ADJ400_FINCA_REGISTRAL,''),
		isnull(ADJ401_REFERENCIA_CATASTRAL,'')
FROM SVH_REPORT_DB.JUNO.MAESTRO_FINCAS
WHERE PERIODO = @ANYOMES