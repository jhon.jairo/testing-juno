USE [SVH_REPORT_DB]
GO



/*
|| Tabla "parche" donde se pondrán los valores que lo campos deben de tener quedamos a fuegos
*/

/*
|| El insert de la data se encuenta en la pestaña "Tabla_C18_adhoc" del excel de definición de campos, compartido en el Teams
*/

/*
EXEC SP_RENAME '[JUNO].[TMP_URS_EXPERTIS]', 'TMP_URS_DATA_ADHOC'
*/

DROP TABLE IF EXISTS [JUNO].[TMP_URS_DATA_ADHOC]

CREATE TABLE [JUNO].[TMP_URS_DATA_ADHOC](
	[UNIDAD_REGISTRAL] INT NULL,
	[SOCIEDAD] INT NULL,
	[ORIGEN] [varchar](50) NULL,
	[USR061] [varchar](50) NULL,
	[USR001] [varchar](50) NULL,
	[ADJ308] [varchar](50) NULL,
	[USR021] [varchar](50) NULL,
	[CONS10] [varchar](50) NULL
)

