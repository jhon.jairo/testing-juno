USE [SVH_REPORT_DB]
GO

DROP TABLE IF EXISTS [JUNO].[TMP_PARAMETRICA]

CREATE TABLE [JUNO].[TMP_PARAMETRICA] (
	Informe nvarchar(255)  NULL,
	Concepto nvarchar(255)  NULL,
	Campo nvarchar(255)   NULL,
	Flag int NULL,
	Valor nvarchar(255)   NULL
)