DROP TABLE IF EXISTS SVH_REPORT_DB.JUNO.INF_FINCAS

CREATE TABLE SVH_REPORT_DB.JUNO.INF_FINCAS (
PERIODO INT,
PROYECTO	VARCHAR(10),
TIPO	VARCHAR(2),
CLAVE1	VARCHAR(4),
CLAVE2	VARCHAR(4),
CLAVE3	VARCHAR(4),
CLAVE4	VARCHAR(4),
ADJ317	VARCHAR(25),
ADJ395	VARCHAR(14),
ADJ396	VARCHAR(2),
ADJ510	VARCHAR(4),
ADJ397	VARCHAR(50),
ADJ398	VARCHAR(14),
ADJ399	VARCHAR(14),
ADJ400	VARCHAR(20),
ADJ401	VARCHAR(110),
USR004  VARCHAR(25)
)
