
USE SVH_REPORT_DB
GO

	/*
	|| Autor: Jhon Reyes
	|| Fecha: 2022.04.11
	|| Proyecto: JUNO
	|| Proceso: Actualizar la tabla MAESTRO_CARTERA, con los valores del cierre contable. 
	||				Solo se actualizaran los campos que dependen del cierre contable		
	*/


	--=========================================================================================================================================
	--														         UPDATE CIERRE CONTABLE - MAESTRO_CARTERA
	--=========================================================================================================================================

	/*
	|| Modificaciones.
	||		- El campo ADJ323 y USR060 se calculan de la misma manera
	*/

	/*
	|| Modificaciones
	|| Fecha: 2022.05.06
	|| Motivo:
	|| 	- Provisonalmente se ha creado variable de fecha de cierre contable. Lidia nos ha pedido que la fecha de cierre contable sea de marzo, y no la de abril.
	*/

	DECLARE @ANYOMES VARCHAR(6);
	SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);


	--=======================================================================
	-- 							TEMPORALES
	--=======================================================================

	DROP TABLE IF EXISTS #TMP_SOCIEDADES

	SELECT VALOR AS SOCIEDAD
	  INTO #TMP_SOCIEDADES
	  FROM [JUNO].[TMP_PARAMETRICA]
	WHERE INFORME = 'SOCIEDAD'
	  AND CONCEPTO = 'KUTXABANK. PERIMETRO BANCOS'
	  AND FLAG = 1
	  
	--=======================================================================
	-- 							UPDATES
	--=======================================================================  

	UPDATE CARTERA
	SET
	CARTERA.USR067_VALOR_CONTABLE_BRUTO             = CIERRE.VALOR_CONTABLE_BRUTO,
	CARTERA.ADJ323_CORRECCIONES_VALOR_POR_DETERIORO = ABS(CIERRE.PROVISION_ADICIONAL + CIERRE.PROVISION_DETERIORO),
	CARTERA.USR043_AMORTIZACION                     = ABS(CIERRE.AMORTIZACION),
	CARTERA.ADJ007_VALOR_CONTABLE_BRUTO					= CIERRE.VALOR_CONTABLE_BRUTO,
	CARTERA.USR044_VALOR_CONTABLE_NETO					= CIERRE.VALOR_CONTABLE_NETO,
	CARTERA.USR060_DETERIORO_TOTAL_CONSOLIDADO		= ABS(CIERRE.PROVISION_ADICIONAL + CIERRE.PROVISION_DETERIORO),
	CARTERA.USR059_INCREMENTO_VALOR_CONSOLIDADO     = CIERRE.IMPORTE_INCREMENTO_VALOR
	-- ... (aquí se irán poniendo el resto de campo que dependen del cierre contable de la tabla UR_VALORES_CONTABLES_CIERRE)
	FROM [JUNO].[MAESTRO_CARTERA] CARTERA
	INNER JOIN [JUNO].[TMP_UR_MAESTRO_HIST] UR ON CARTERA.SOCIEDAD = UR.SOCIEDAD AND CARTERA.UNIDAD_REGISTRAL = UR.UNIDAD_REGISTRAL
	INNER JOIN #TMP_SOCIEDADES SOC ON CARTERA.SOCIEDAD = SOC.SOCIEDAD
	INNER JOIN [SVH_CORP_DB].[dbo].[UR_VALORES_CONTABLES_CIERRE] CIERRE ON CARTERA.SOCIEDAD = CIERRE.SOCIEDAD AND CARTERA.UNIDAD_REGISTRAL = CIERRE.UNIDAD_REGISTRAL
	WHERE CARTERA.CONTROL_CARTERA = 'STOCK' 
		AND CIERRE.ANYO_MES = @ANYOMES
		AND CARTERA.PERIODO = @ANYOMES
		AND UR.IND_MIGRACION = 'X'
	 

	/*
	|| UPDATE ESPECIFICO PARA BANCOS USR059. No tira de la tabla de VALORES_CONTABLES_CIERRE pero si se calcula a partir de un campo extraido de dicha tabla
	*/
		
	UPDATE CARTERA
	SET USR059_INCREMENTO_VALOR_CONSOLIDADO = ADJ007_VALOR_CONTABLE_BRUTO - USR058_VALOR_ADQUISICION_CONSOLIDADO - USR017_IMPORTE_MENOR_COSTE_INICIAL
	FROM  [JUNO].[MAESTRO_CARTERA] CARTERA
	INNER JOIN [JUNO].[TMP_UR_MAESTRO_HIST] UR ON CARTERA.SOCIEDAD = UR.SOCIEDAD AND CARTERA.UNIDAD_REGISTRAL = UR.UNIDAD_REGISTRAL
	INNER JOIN #TMP_SOCIEDADES SOC ON CARTERA.SOCIEDAD = SOC.SOCIEDAD          
	WHERE CARTERA.PERIODO = @ANYOMES
	AND UR.IND_MIGRACION = 'X'


