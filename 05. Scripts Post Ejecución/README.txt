Carpeta: "01. Ejecutar"
	
	OLD
		Scripts:
			- Ejecutar en Mayo:
				- 01. JUNO - DELETE 152 ACTIVOS - INF ADJUDICADOS.sql
			- Ejecutar en Junio:
				- 01. JUNO - DELETE 152 ACTIVOS - INF ADJUDICADOS.sql
				- 02. JUNO - DELETE 135 ACTIVOS - INF ADJUDICADOS.sql
			- Ejecutar en Julio:
				- 01. JUNO - DELETE 152 ACTIVOS - INF ADJUDICADOS.sql
				- 02. JUNO - DELETE 135 ACTIVOS - INF ADJUDICADOS.sql
				- 03. JUNO - DELETE 17 ACTIVOS - INF ADJUDICADOS.sql
			
	NEW!!		
			
		1. [OK] Crear tabla
		2. Insertar URs a borrar en la tabla creada. 
				Ejecutar mes a mes. Si no hay nuevos activos que borrar, se puede dejar de ejecutar, pero para que entre en la dinamica 
				de ejecución de los cierres, se añade al circuito. Si luego habrían nuevos activos a borrar, se tiene que modificar este script,
				para añadirlos. Esa es una de las razones por la que se decide ejecutar mes a mes.
				
			- "01. JUNO - INSERT TABLE DELETE URS.sql"
		3. Ejecutar en cada cierre el borrado, mediante un DELETE haciendo join entre la tabla de borrado y la inf adjudicados
			- "02. JUNO - DELETE URS - INF ADJUDICADOS.sql"
		
	
	Query Tabla Borrado:

		SELECT
		  MES_INICIO_BORRADO,
		  CODIGO_ACTUACION_BAJA,
		  DESC_CODIGO_ACTUACION_BAJA,
		  CONTROL_CARTERA,
		  COUNT(1)
		FROM [JUNO].[TMP_INF_ADJUDICADOS_BORRADO_URS]
		GROUP BY MES_INICIO_BORRADO,
					CODIGO_ACTUACION_BAJA,
					DESC_CODIGO_ACTUACION_BAJA,
					CONTROL_CARTERA
		ORDER BY 1, 5 DESC		
		