
/*
|| Autor: Jhon Reyes C.
|| Fecha: 2022.08.11
|| Proceso: Borrar activos que por error se han dado de alta.
|| Detalle Proceso:
|| 	Script que borra los activos de la tabla inf adjudicados. Este script se ejecuta en cada mes de cierre.
|| 	Estos activos en su momento Laura comentó que se deberían de borrar. Este borrado se viene realizando desde mayo,
|| 	que es el primer cierre que se ha realizado desde SVH.
|| 	En cada cierre han existido nuevos activos(URs) a borrar.  
*/

use SVH_REPORT_DB
go

DECLARE @PERIODO_DATAMART VARCHAR(6)
SET @PERIODO_DATAMART = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);

DELETE ADJ
FROM [JUNO].[INF_ADJUDICADOS] ADJ
INNER JOIN [JUNO].[TMP_INF_ADJUDICADOS_BORRADO_URS] BORRADO ON ADJ.SOCIEDAD = BORRADO.SOCIEDAD AND ADJ.UNIDAD_REGISTRAL = BORRADO.UNIDAD_REGISTRAL
WHERE ADJ.PERIODO = @PERIODO_DATAMART AND BORRADO.IND_BORRADO = 'X'


