
USE SVH_REPORT_DB
GO


/*
|| Autor: Jhon Reyes
|| Fecha: 2022.09.20
|| Proceso: Se insertan las nuevas URs que se quieren incluir en los informes de adjudicados.
||				Esto se puede dar por que en el momento del cierre estas URs no cumplian criterio para migrarlas.
||				Si se llega a ejecutar este script, lo normal es que sea en días D+X, ya que en el día D no cumplian criterio.
||	A tener en cuenta, que este proceso es post migración de maestro cartera.
||		Ejecutar:
||			1. "00. Insert URs a incluir en el perimetro.sql". Insertar en la tabla TMP_MODIFICACION_PERIMETRO_CIERRE, las URs que se quieren incluir en el perímetro
||			2. "01. Proceso Incluir URs al Perimetro.sql". Insertar en la tabla TMP_UR_MAESTRO_HIST, las URs que previamente se insertaron en el punto 1
||				2.1. En este proceso se actualiza el campo/flag IND_MIGRACION. 
||						Para las URs ya migradas se actualiza/desactiva el flag = '', para las nuevas URS del punto 1, se insertan con el flag activado = 'X'
||			3. "07. JUNO. MAESTRO_CARTERA.sql". Inserta en la tabla MAESTRO_CARTERA, las nuevas URs
||			4. Si aplica ejecutar los procesos de la carpeta "04. Procesos Post DTM" 
*/


--========================================================================================================================================
--																		PERIMETRO. Incluir URs		
--========================================================================================================================================


DECLARE @ANYOMES VARCHAR(6);
SET @ANYOMES = CONVERT(VARCHAR(6),DATEADD(dd,-(DAY(GETDATE())),GETDATE()),112);

INSERT INTO [JUNO].[TMP_MODIFICACION_PERIMETRO_CIERRE](SOCIEDAD, UNIDAD_REGISTRAL, CONTROL_PERIMETRO, PERIODO_CIERRE, COMENTARIOS, IND_EJECUCION, FECHA_CARGA)
VALUES( , , 'INCLUIR URS - UR_MAESTRO', @ANYOMES, 'Extracción de los datos desde UR_MAESTRO', 'X', CAST(GETDATE())


