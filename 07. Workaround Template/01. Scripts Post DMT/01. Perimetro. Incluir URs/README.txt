
Fecha: 	2022.09.14
Autor: 	Jhon Reyes
Uso: 		Script que inserta las nuevas URs que se deben de migrar al datamart de cartera.
Proceso: Inserta en la tabla de perimetros las nuevas urs a migrar. 
			Estas nuevas URs se insertan con el flag de migración activado('X'), mientras que los registros que ya se han migrado
			a maestro cartera, se les desactiva el flag.

			Detalle de la tabla perimetro [JUNO].[TMP_UR_MAESTRO_HIST]:
				- Flag activado cuando: IND_MIGRACION = 'X'
				- Flag desactivado cuando: IND_MIGRACION = ''


A tener en cuenta: Aquí los datos se obtienen de la tabla UR_MAESTRO, mientras que en el proceso normal se obtienen del historico de UR_MAESTRO


Execution Flow:


	1. "00. Insert. URs a incluir al perimetro.sql". Insertar en la tabla TMP_MODIFICACION_PERIMETRO_CIERRE, las URs que se quieren incluir en el perímetro
	2. "01. Proceso. URs a incluir al perimetro.sql". Insertar en la tabla TMP_UR_MAESTRO_HIST, las URs que previamente se insertaron en el punto 1
		2.1. En este proceso se actualiza el campo/flag IND_MIGRACION. 
				Para las URs ya migradas se actualiza/desactiva el flag = '', para las nuevas URS del punto 1, se insertan con el flag activado = 'X'
	3. "07. JUNO. MAESTRO_CARTERA.sql". Inserta en la tabla MAESTRO_CARTERA, las nuevas URs
		3.1. En este proceso ya se filtra por el flag activado del campo IND_MIGRACION
	4. Si aplica ejecutar los procesos de la carpeta "04. Procesos Post DTM"